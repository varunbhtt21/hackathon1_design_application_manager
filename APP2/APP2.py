from confluent_kafka import Producer 
from confluent_kafka import Consumer, KafkaError 

PATH = "/home/varun/Desktop/Application_Manager/APP2/"


p = Producer({'bootstrap.servers': 'localhost:9092'})

def acked(err, msg):
	if err is not None:
		print("Failed to deliver message: {0}: {1}".format(msg.value(), err.str())) 

def producer(val):
	try:
		p.produce('App_Manager', val, callback=acked)
		p.poll(0.5)

	except KeyboardInterrupt:
		pass

	p.flush(30)


producer(PATH)