import base64
from confluent_kafka import Producer
from confluent_kafka import Consumer, KafkaError
import threading 
import schedule 
import time 

settings = {
	'bootstrap.servers': 'localhost:9092',
	'group.id': 'mygroup',
	'client.id': 'client-1',
	'enable.auto.commit': True,
	'session.timeout.ms': 6000,
	'default.topic.config': {'auto.offset.reset': 'smallest'}
}


c = Consumer(settings)
c.subscribe(['App_Manager'])
p = Producer({'bootstrap.servers': 'localhost:9092'})

def acked(err, msg):
	if err is not None:
		print("Failed to deliver message: {0}: {1}".format(msg.value(), err.str())) 

def producer(port, path):
	try:
		p.produce('Deployer', str(port)+":"+path, callback=acked)
		p.poll(0.5)

	except KeyboardInterrupt:
		pass

def Deployer(port, PATH):
	p.flush(30)
	producer(port, PATH)
	

def load_balancer(PATH):
	port = 8080
	Deployer(port, PATH)
	

def Scheduler(PATH):
	PATH_m = PATH+ "meta_data.txt"
	print("\n\n Scheduler Started \n\n")
	
	file = open(PATH_m, 'r')
	Lines = file.readlines()

	for line in Lines:
		if "Start" in line:
			start_time = line.split("~")[1].strip()
		
		if "End" in line:
			end_time = line.split("~")[1].strip()

		if "Location" in line:
			location= line.split("~")[1].strip()

		if "Temperature" in line:
			temperature= line.split("~")[1].strip()

			
	print("\n Start Time : "+str(start_time))
	# print("\n End Time : "+str(end_time))
	
	PATH_c= PATH+ "config.txt"
	# available= schedule.every().day.at(start_time).do(load_balancer) 
	schedule.every(0.1).minutes.do(lambda: load_balancer(PATH_c)) 
	# print (available)

	while True: 
		# Checks whether a scheduled task  
		# is pending to run or not 
		schedule.run_pending() 
		time.sleep(1) 
		
		



def App_Manager():
	print("App Manager Started ... \n")
	while True: 
		while 1:
			msg = c.poll(0.1)
			if msg is None:
				continue
			elif not msg.error():
				path = msg.value().decode("utf-8")
				break  

		print("Thread Shoot")
		print(path)
		t1 = threading.Thread(target = Scheduler, args=(path,)) 
		t1.start() 


App_Manager()
  

