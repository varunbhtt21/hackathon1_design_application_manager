import base64
from confluent_kafka import Producer
from confluent_kafka import Consumer, KafkaError
import threading 
import schedule 
import time 

settings = {
	'bootstrap.servers': 'localhost:9092',
	'group.id': 'mygroup',
	'client.id': 'client-1',
	'enable.auto.commit': True,
	'session.timeout.ms': 6000,
	'default.topic.config': {'auto.offset.reset': 'smallest'}
}



c = Consumer(settings)
c.subscribe(['Deployer'])
p = Producer({'bootstrap.servers': 'localhost:9092'})

def acked(err, msg):
	if err is not None:
		print("Failed to deliver message: {0}: {1}".format(msg.value(), err.str())) 

def producer(port, dependencies):
	try:
		p.produce(str(port), dependencies, callback=acked)
		p.poll(0.5)

	except KeyboardInterrupt:
		pass


def server(port, path):
	#check if there are multiple port and check the dependecies on evry server(not done now)
	# produce on the ip:port of the test server
	# produce the java:windows
	file = open(path, 'r')
	Lines = file.readlines()
	flag= 0

	path_list= path.split("/")
	path_list= path_list[:-1]
	path= "/".join(path_list)

	for line in Lines:

		if flag ==0:
			code_file= path+'/'+ line
			flag =1
	
		if "Platform" in line:
			platform = line.split("~")[1].strip()
		
		if "OS" in line:
			os = line.split("~")[1].strip()

	dependencies= code_file + ":" +platform + ":"+ os 
	p.flush(30)
	print("\n\n Path send to Node : "+dependencies)
	producer(port, dependencies)



def Deployer():
	print ("Deployer Running ...\n\n")
	while True: 
		while 1:
			msg = c.poll(0.1)
			if msg is None:
				continue
			elif not msg.error():
				message = msg.value().decode("utf-8")
				break  

		print("Received message= ", message)
		port= message.split(":")[0]
		path= message.split(":")[1]

		print ("Port = ", port)
		print ("Path = ", path)
		t1 = threading.Thread(target = server, args=(port, path)) 
		t1.start() 

Deployer()
  

